const jwtDecode = require('jwt-decode')

module.exports = function permission(userRole) {
    return function(req, res, next) {
        const token = req.header("Authorization");
        if(!token) {
            return res.status(401).json({
                message: "unauthorized user"
            })
        }

        try {
            let { userType } = jwtDecode(token);
            let isIncluded = userRole.includes(userType);
            if(isIncluded) next();
            else {
                next(res.status(401).json({
                    message:"You are not an authorized user"
                }))
            }
        }
        catch(error) {
            res.json({
                message: "You do not have permission for this API",
                error
            })
        }
    }
}
