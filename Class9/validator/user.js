const Joi = require('joi');

const addressSchema = Joi.object({
      division: Joi.string(),
      district: Joi.string()
  })

const userValidator = Joi.object({
  firstName: Joi.string().min(2).max(15),
  userName: Joi.string().required().alphanum(),
  userType: Joi.string(),
  email: Joi.string()
    .trim()
    .required()
    .regex(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    ),
  password: Joi.string().required().pattern(new RegExp("^[a-zA-Z0-9]{6,12}$")),
  image: Joi.string(),
  address: addressSchema
});


module.exports = {
    userValidator
}