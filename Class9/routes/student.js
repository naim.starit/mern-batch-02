const express = require('express');
const router = express.Router();
const isAuth = require('../middleware');

const {
  getAll,
  getById,
  register,
  login,
  update,
  temporeryDelete,
  restore,
  deleteStudent,
  forgotPassword,
  checkOTP,
  resetPassword,
} = require("../controller/student");

router.get('/student/:id([0-9]{4})', getById );
router.get("/all-student", isAuth, getAll);
router.post("/student/register", register);
router.post("/student/login", login);
router.put("/student/update/:id", update);
router.put("/student/temp-delete/:id", temporeryDelete);
router.put("/student/restore/:id", restore);
router.delete("/student/delete/:id", deleteStudent);
router.post("/student/forgot-password", forgotPassword);
router.post("/student/check-otp", checkOTP);
router.post("/student/reset-password", resetPassword);

// 'student/rename/:id/:name'

module.exports = router;

