const express = require('express');
const router = express.Router();
const fileUploader = require('../middleware/fileUploader');

const {
    singleFileUploader,
    multipleFileUploader,
    imageUpload
} = require('../controller/fileUpload');

router.post('/single-upload', fileUploader.single('image'), singleFileUploader);
router.post('/multiple-upload',fileUploader.array('images', 5), multipleFileUploader);
router.post("/image-upload", imageUpload);
module.exports = router;