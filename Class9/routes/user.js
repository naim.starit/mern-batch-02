const express = require("express");
const router = express.Router();

const Auth = require('../middleware/auth');
const permission = require('../middleware/permission');

const {
  getAll,
  getById,
  register,
  login,
  temporeryDelete,
  restore,
  deleteUser,
} = require("../controller/user");

router.get('/getAll',Auth, permission(['teacher', 'student']), getAll);
router.get('/get-by-id/:id', getById);
router.post('/register', register);
router.post('/login', login);
router.put("/temp-delete", temporeryDelete);
router.put("/restore", restore); 
router.delete("/delete", deleteUser); 

module.exports = router;
