const express = require('express');
const app = express();
// all routes
const studentRoutes = require('./routes/student');
const userRoutes = require('./routes/user');
const uploaderRoutes = require('./routes/fileUpload')

const isAuth = require('./middleware');
var morgan = require("morgan");
var rateLimit = require("express-rate-limit");
const mongoose = require('mongoose');
require('dotenv').config();
// app.use(isAuth);

var limiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 5,
    message: "Too many request, Please try some times later"
})

app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ limit: "10mb" }));
app.use(express.static("images"));
// app.use(limiter);
app.use(morgan('tiny'))
app.use(studentRoutes);
app.use('/user', userRoutes);
app.use("/upload", uploaderRoutes);

app.get('/hello', (req, res)=> {
     res.send("hello");
})

app.get('*', (req, res)=> {
    res.send('<h1>No API found with this route</h1>')
})

//Database connect
mongoose.connect('mongodb://localhost:27017/mern02').then(()=> console.log('Database connected'))
.catch((err)=> console.log(err))

// server
const port = process.env.PORT || 5000;
app.listen(port, ()=>console.log(`Server is listening on port ${port}`))