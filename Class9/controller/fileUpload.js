const Image = require('../model/images');
const fs = require('fs');
const baseUrl = 'http://localhost:3000';

const singleFileUploader = async(req, res) => {
   try {
        if(req.file) {
        res.json({
            message: "file upload successfully",
            fileDetails: req.file
        })
    }
    else {
        res.json({
            message: 'upload a valid image'
        })
    }
   }
   catch(error) {
       res.json({
           error
       })
   }
}

const multipleFileUploader = (req, res) => {
    console.log(req.files);
    res.send('file uploaded');
}

const imageUpload = async(req, res)=> {
    try{
        let image = req.body.image;
        // data:image/png;base64,iVBORw0KGgoAAAANSUhEUg......
        let data = image.split(';base64,');
        let base64Data = data[1];
        let imageExtension = data[0].split('/')[1];
        let fileName = `image${+new Date()}.${imageExtension}`;

        let pathName = `${baseUrl}/${fileName}`;

        fs.writeFile(`${__dirname}/../images/${fileName}`, base64Data, {encoding: 'base64'}, function(err){
            if(err) {
                console.log(err)
            }
            else {
                console.log('File has been successfully saved!!!')
            }
        });

        req.body.image = pathName;
        let img = new Image(req.body);
        let result = await img.save();

        if(result) {
            res.status(201).json({
                message: 'Image added successfully',
                data: result
            })
        }


    }
    catch(error) {
        res.json({
            error
        })
    }
}

module.exports = {
    singleFileUploader, 
    multipleFileUploader,
    imageUpload
}