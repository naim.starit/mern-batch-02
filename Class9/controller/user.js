const User = require('../model/user');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { userValidator } = require('../validator/user');
//========================================
//============= Get by id ================
//========================================
const getById = (req, res) => {
  try{
      const id = req.params.id;
      const user = User.findOne({_id: id});
      if(user) {
          return res.json({
              data: user
          })
      }
      else {
        return res.json({
            message: "User not found"
        })
      }
  }catch (error) {
    res.json({
      error,
    });
  }
};

//========================================
//============= Get All ==================
//========================================
const getAll = async (req, res) => {
  try {
    const data = await User.find({ isDeleted: false });
    if (data.length) {
      return res.json({
          data
      });
    } else {
      return res.json({
          message: "User not found"
      })
    }
  } catch (error) {
    res.json({
      error,
    });
  }
};

//========================================
//============= Register ================
//========================================

const register = async(req, res) => {
    try {
      const {error, value} = userValidator.validate(req.body);
      if(error) {
        res.status(400).json({
          message: "validation error",
          error: error.details[0].message
        })
      }
      else {
        const user = new User(req.body);
        const data = await user.save();

        return res.status(201).send({
          msg: "user added successfully",
          data,
        });
      }
    } catch (error) {
      res.json({
        error,
      });
    }
}
//========================================
//============= Login ====================
//========================================
const login = async (req, res) => {
  const { email, password } = req.body;
  let user = await User.findOne({ email });
  if (user) {
    const isValid = await bcrypt.compare(password, user.password);
    if (isValid) {
      const data = {
        email: user.email,
        userName: user.userName,
        userType: user.userType
      };

      const token = jwt.sign(data, process.env.SECRETE_KEY, {
        expiresIn: "5d",
      });
      res.json({
        message: "login successfull",
        token,
      });
    } else {
      res.json({
        message: "password doesn't match",
      });
    }
  } else {
    res.json({
      msg: "user not found",
    });
  }
};

//========================================
//============= temporeryDelete ==========
//========================================
const temporeryDelete = async (req, res) => {
  const id = req.params.id;
  await User.findOneAndUpdate(
    { _id: id },
    {
      $set: { isDeleted: true },
    }
  );
  return res.json({
    message: "User deleted successfully",
  });
};

//========================================
//============= Restor ===================
//========================================
const restore = async (req, res) => {
  const id = req.params.id;
  await User.findOneAndUpdate(
    { _id: id },
    {
      $set: { isDeleted: false },
    }
  );
  return res.json({
    message: "Restored successfully",
  });
};

//========================================
//============= delete user =============
//========================================

const deleteUser = async (req, res) => {
  const id = req.params.id;
  await User.findOneAndDelete({ _id: id });
  return res.json({
    message: "User deleted successfully",
  });
};

module.exports = {
    getById,
    getAll,
    register,
    login,
    temporeryDelete,
    restore,
    deleteUser
}