const Student = require("../model/student");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

const secretKey = "ujsdyhfils38439wehdshfkjh";

const getById = (req, res) => {
  var { id } = req.params;
  res.send(`Your ID is - ${id}`);
};

const getAll = async (req, res) => {
  try {
    const data = await Student.find({ isDeleted: false });
    if (data.length) {
      return res.status(200).send(data);
    } else {
      return res.status(200).send("No Student found");
    }
  } catch (error) {
    res.json({
      error,
    });
  }
};

const register = async (req, res) => {
  try {
    const { password } = req.body;
    let hashedPassword = await bcrypt.hash(password, 10);
    req.body.password = hashedPassword;

    const student = new Student(req.body);
    const data = await student.save();

    return res.status(201).send({
      msg: "student added successfully",
      data,
    });
  } catch (error) {
    res.json({
      error,
    });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  let student = await Student.findOne({ email });
  if (student) {
    const isValid = await bcrypt.compare(password, student.password);
    if (isValid) {
      const data = {
        email: student.email,
        firstName: student.firstName,
      };
      const token = jwt.sign(data, secretKey, { expiresIn: "1h" });
      res.json({
        message: "login successfull",
        token,
      });
    } else {
      res.json({
        message: "password doesn't match",
      });
    }
  } else {
    res.json({
      msg: "Student not found",
    });
  }
};

const update = async (req, res) => {
  const id = req.params.id;
  await Student.findOneAndUpdate(
    { _id: id },
    {
      $set: req.body,
    },
    {
      multi: true,
    }
  );

  return res.status(204).json({
    message: "Student info updated successfully",
    data: req.body,
  });
};

const temporeryDelete = async (req, res) => {
  const id = req.params.id;
  await Student.findOneAndUpdate(
    { _id: id },
    {
      $set: { isDeleted: true },
    }
  );
  return res.json({
    message: "Student deleted successfully",
  });
};
const restore = async (req, res) => {
  const id = req.params.id;
  await Student.findOneAndUpdate(
    { _id: id },
    {
      $set: { isDeleted: false },
    }
  );
  return res.json({
    message: "Restored successfully",
  });
};

const deleteStudent = async (req, res) => {
  const id = req.params.id;
  await Student.findOneAndDelete({ _id: id });
  return res.json({
    message: "Student deleted successfully",
  });
};

const forgotPassword = async (req, res) => {
  try {
    const { email } = req.body;
    const student = await Student.findOne({ email });
    if (!student) {
      return res.status(400).json({
        message: "Student not found",
      });
    }

    let otp1 = Math.floor(Math.random() * 8999) + 1000;
    let otp2 = Math.floor(Math.random() * 89) + 10;
    let otp = "" + otp1 + otp2;

    await Student.updateOne({ otp });

    //============ Nodemailer ===============

    //========== First Step =================
    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "dhakametrootp@gmail.com",
        pass: "*******",
      },
    });

    //============ Second Step ==================

    let mailOptions = {
      from: "dhakametrootp@gmail.com",
      to: email,
      subject: "Reset Password OTP",
      text: "Hello",
      html: `<h3>Copy the code below</h3> <h1 style="color: green;">${otp}</h1>`,
    };

    //================== Third step ====================

    await transporter.sendMail(mailOptions, function (err, data) {
      if (err) {
        return res.json({
          message: "Email sending failed",
          error: err,
        });
      } else {
        return res.json({
          message: "Email has been sent successfully",
        });
      }
    });
  } catch (error) {
    return res.json({
      error,
    });
  }
};

const checkOTP = async (req, res) => {
  try {
    const { otp } = req.body;
    const student = await Student.findOne({ otp });
    if (!student) {
      return res.json({
        message: "Invalid OTP",
      });
    }

    const data = {
      email: student.email
    };

    const token = jwt.sign(data, secretKey, { expiresIn: "1h" });
    return res.json({
        token
    })
  } 
  catch (error) {
    return res.json({
      error,
    });
  }
};

const resetPassword = async(req, res) => {
   try {
     const { newPassword, confirmNewPassword } = req.body;
     if (newPassword !== confirmNewPassword) {
       return res.json({
         message: "Password doesn't match",
       });
     }
     else {
         const {token} = req.query;
         let data = jwt.verify(token, secretKey);

         let hashedPassword = await bcrypt.hash(newPassword, 10);
         
         await Student.findOneAndUpdate(
           { email: data.email },
           {
             $set: { password: hashedPassword, otp: "" },
           },
           {
               multi: true
           }
         );
           return res.json({
               message: "Password has been updated successfully"
           })
     }
   } catch (error) {
     return res.json({
       error,
     });
   }
}

module.exports = {
  getById,
  getAll,
  register,
  login,
  update,
  restore,
  temporeryDelete,
  deleteStudent,
  forgotPassword,
  checkOTP,
  resetPassword,
};
