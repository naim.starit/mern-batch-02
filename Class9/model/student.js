const mongoose = require('mongoose');
const { Schema } = mongoose;

const studentSchema = new Schema({
    firstName: String,
    middleName: {
        type: String
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: String,
    isDeleted: {
        type: Boolean,
        default: false
    },
    otp: ""
});

module.exports = mongoose.model('student', studentSchema);