const http = require('http');
const path = require('path');  

const os = require('os');

// const data = require('../info');

http.createServer(function(req, res) {
  //baseName
  // const fullPath = '/c/class5/hello.js';
  // const fileName = path.basename(fullPath);
  // console.log(fileName);

  //dirName
//   const fullPath = '/c/class5/hello.js';
//   const dirName = path.dirname(fullPath);
//   console.log(dirName);

  // extension name
    // const extName = path.extname('Hello.module.js');
    // console.log(extName);

    // path format 
    // const dir = 'c\\class5';
    // const fileName = 'hello.js';

    // const format = path.format({
    //     dir,
    //     base: fileName 
    // })
    // console.log(format);

    // isAbsolute 

    // const dir = 'hello.js';
    // const result = path.isAbsolute(dir);
    // console.log(result);

    // join 

    // const fullPath = path.join('/home', 'node', 'class5', 'hello.txt');
    // console.log(fullPath);

    // let fullPath = 'C:/home/node/class5/hello.js';
    // console.log(path.parse(fullPath));

    // console.log(__dirname);
    // console.log(path.resolve());
    // let pathName = path.resolve("/bar/class6", "hello.html"); //path.resolve('js', 'hello.js');
    // console.log(pathName);

    
// Returns: '/tmp/file'


//=================== OS MODULE ====================

let osName = os.type();
let architecture = os.arch();
let platform = os.platform();
let release = os.release();
let version = os.version();
let user = os.userInfo();

let totalMemory = os.totalmem();

let cpuInfo = os.cpus();
console.log(cpuInfo);


}).listen(3000);

console.log('Server is lisitening');

// const arr = [
//     {
//         totalMoney: 5000,
//         male: 10, // percentage 
//         female: 15
//     },
//     {
//         className: 1,
//         scholership: '',
//         gender: 'Male'
//     },
//     {
//         className: 2,
//         scholership: '',
//         gender: 'feMale'
//     },
//     {
//         className: 2,
//         scholership: '',
//         gender: 'male'
//     },
//     {
//         className: 1,
//         scholership: '',
//         gender: 'female'
//     }
// ]

// Raju Molla10:38 PM

// var len=arr.length;
// for(var i=0; i<len; i++){
//     if(arr[i].gender==='male' ||arr[i].gender==='Male'){
//         arr[i].scholership=arr[0].totalMoney/arr[0].male;
//     }
//     else if(arr[i].gender==='female' || arr[i].gender==='feMale'){
//         arr[i].scholership=arr[0].totalMoney/arr[0].female;
//     }
// }
// console.log(arr);

// arr.map((item) => {
//   if (item.gender === "male" || item.gender === "Male") {
//     return (item.scholership = (5000 * 10) / 100);
//   } else {
//     return (item.scholership = (5000 * 15) / 100);
//   }
// });
// console.log(arr);

// arr.forEach((item) => {
//   if (item.gender != undefined) {
//     let g = item.gender;
//     if (g.toLowerCase() == "male") {
//       item.scholership = 5000 * 0.1;
//     } else {
//       item.scholership = 5000 * 0.15;
//     }
//   }
// });
// console.log(arr);

// express.js   // hapi.js   // koa.js  // fstify  //nest.js