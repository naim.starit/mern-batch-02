const express = require('express');
const app = express();


app.get('/', (req, res)=> {
     res.send('hello');
})

app.get('/all-student', (req,res)=> {
    const userId = req.query.id;

    const allStudent = [
        {
            id:1,
            name: "Karim",
            age: 12
        },
        {
            id:2,
            name: "Rahim",
            age: 13
        },
        {
            id:3,
            name: "Pori Moni",
            age: 16
        },
        {
            id:4,
            name: "Sefuda",
            age: 10
        },
        {
            id:5,
            name: "Hiru Alam",
            age: 17
        },
    ]

    if(userId) {
        let uniqueStudent = allStudent.filter((ele) => ele.id == userId);
        if (uniqueStudent.length) {
            res.status(200).send(uniqueStudent[0]);
        }
        else {
            res.send(`No student found with id - ${userId}`)
        }
    }
    res.status(200).send(allStudent);
})

app.get('*', (req, res)=> {
    res.send('<h1>No API found with this route</h1>')
})

const port = process.env.PORT || 3000;
app.listen(port, ()=>console.log(`Server is listening on port ${port}`))