// let student = {
//     name: 'Next',
//     age: 12,
//     color: 'white',
//     weight: 42,
//     roll: 5,
//     eyeColor: 'black',
//     parent: {
//         fathersName: 'Mr. X',
//         mothersName: 'Mrs. Y'
//     }
// }

// let anotherStudent = { ...student };

// delete student.age;

//  console.log(anotherStudent);

// var num = 10;
// var num1 = num;
// num = 20;
// console.log(num1);


// var arr = [2, 5, 6, 8];
// var copyArr = [ ...arr ];
// arr.push(100)
// console.log(copyArr);

// destructing 
// let student = {
//     name: 'Next',
//     age: 12,
//     color: 'white',
//     weight: 42,
//     roll: 5,
//     eyeColor: 'black',
//         parent: {
//         fathersName: 'Mr. X',
//         mothersName: 'Mrs. Y'
//     }
// }

// let { age:myAge, color } = student;
// let { fathersName, mothersName } = student.parent;

// console.log(fathersName, color);

// var arr = [5, 2, 6, 10, 15, 45];

// var [a, b, ...rest] = arr;
// console.log(b, rest);


// let student = {
//     name: 'Next',
//     age: 12,
//     color: 'white',
//     weight: 42,
//     roll: 5
// }

// for(let key in student) {
//     // console.log(typeof student[key]);
//     if(typeof student[key] == 'number') {
//         student[key] *= 2;
//     }
// }

// console.log(student);

// optinal chainning 

// let student = {
//   name: "Next",
//   age: 12,
//   color: "white",
//   weight: 42,
//   roll: 5,
//     parent: {
//     fathersName: 'Mr. X',
//     mothersName: 'Mrs. Y',
//         hello: {
//             phone: '017255554'
//         }
//     }
// }

// console.log(student?.parent?.info?.phone);

// var arr = [
//   {
//     code: "101",
//     price: 50,
//   },
//   {
//     price: 60,
//   },
//   {
//     code: "101",
//     price: 80,
//   },
//   {
//     code: "102",
//     price: 500,
//   },
// ];

//  let totalPrice = arr.filter(x => x.code == '101' ).reduce((acc, currVal) => {
//    return acc + currVal.price;
//  }, 0);

//  console.log(totalPrice);
// let std = {
//   name: "Mr. S",
//   age: 25,
//   known: ["HTML", "CSS", "JS"],
// };

// let anotherStd = { ...std };

// std.known.push("php");

// console.log(std);
// console.log(anotherStd);
