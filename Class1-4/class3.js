//JSON 

// var student = {
//     name: 'Next Topper',
//     age: 12,
//     isDeleted: false,
//     hobby: ['travelling', 'shopping', 'sleeping']
// }

// var res = JSON.stringify(student);

// var returnToObj = JSON.parse(res);
// console.log(typeof returnToObj);

// let std = {
//   name: "Mr. S",
//   age: 25,
//   known: ["HTML", "CSS", "JS"],
// };

// // let anotherStd = { ...std };
// let copyStd = JSON.parse(JSON.stringify(std));

// std.known.push("php");

// console.log(std);
// console.log(copyStd);


// Promise 

// function takeBreakfast() {
//     return new Promise((resolve, reject) => {
//         setTimeout(()=> {
//             resolve('Take Breakfast');
//         }, 5000)
//     })
// }

// function goToOffice() {
//     return new Promise((resolve, reject) => {
//       setTimeout(() => {
//         resolve("Go to office");
//       }, 3000);
//     });
// }

// async...await
// async function execute() {
//     // goToOffice().then((res) => console.log(res));
//     // takeBreakfast().then(res=>console.log(res));
//      let res1 = await goToOffice();
//      console.log(res1);

//     let res = await takeBreakfast();
//     console.log(res);
// }
// execute();
// takeBreakfast()
// .then(res=> {
//     console.log(res);
// })
// .catch(err=>{
//     console.log("from catch block: ", err);
// })


var arr = [
  {
    product: "a",
    weight: 102,
    description: {
      label: {
        name: "a",
      },
    },
  },
  {
    product: "b",
    weight: 105,
    description: {
      label: {
        name: "b",
      },
    },
  },
  {
    product: "c",
    weight: 107,
    description: {
      data: {
        name: "c",
      },
    },
  },
  {
    product: "d",
    weight: 109,
    description: {
      label: {
        name: "d",
      },
    },
  }
];

var label = [], weight = [];
arr.map(ele=>{
    if(ele?.description?.label?.name) {
        label.push(ele?.description?.label?.name);
    }
    if(ele.product === 'a' || ele.product === 'c') {
        weight.push(ele.weight)
    }
})
console.log('Label = ', label);
console.log('Weight = ', weight);

// label = ['a', 'b', 'd']
// weight = [102, 107]